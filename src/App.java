
public class App {

	public static void main(String[] args) {
		Maquina maquina = new Maquina(Integer.MAX_VALUE);
		maquina.sortear();
		maquina.calcularPontuacao();
		
		Impressora.imprimir(maquina);

	}

}
